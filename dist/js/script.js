// //..............percentage....
const counters = document.querySelectorAll('.skills__rating-counter'),
      lines = document.querySelectorAll('.skills__rating-line span');

counters.forEach( (item, i) => {
    lines[i].style.width = item.innerHTML;
});
// //.............menu........
const ham = document.querySelector('.hamburger'),
      nav = document.querySelector('.navigation'),
      link = document.querySelectorAll('.navigation__link'),
      toggle = document.querySelector('.toggle');

    ham.addEventListener('click', () => {
        nav.classList.toggle('navigation_active');
        ham.classList.toggle('hamburger__arrtoleft-active');
    });
    toggle.addEventListener('click', () => {
        nav.classList.toggle('navigation_active');
        toggle.classList.toggle('toggle_active');
    });

    link.forEach(item => {
        item.addEventListener("click", () =>{
            if(nav.classList.contains('navigation_active')){
                toggle.classList.toggle('toggle_active');
                nav.classList.toggle('navigation_active');
            }
        ham.classList.remove('hamburger__arrtoleft-active');
        });
    });


// ..........tabs..........
const tabs = document.querySelectorAll('.experience__btn');
const tabsContent = document.querySelectorAll('.experience__wrapper');
const tabsParent = document.querySelector('.experience__buttons');

function hideTabContent() {
    tabsContent.forEach(item => {
        item.classList.add('hide');
        item.classList.remove('show');
    });
    tabs.forEach(item => {
        item.classList.remove('experience__btn_active');
    });
}

function showTabContent(i = 0){
    tabsContent[i].classList.add('show');
    tabsContent[i].classList.remove('hide');

    tabs[i].classList.add('experience__btn_active');
}
hideTabContent();
showTabContent();

tabsParent.addEventListener('click', (event) => {
    const target = event.target;
    if(target && target.classList.contains('experience__btn')){
        tabs.forEach((item, i) => {
            if(target == item){
                hideTabContent();
                showTabContent(i);
            }
        });
    }
});

//...........button to up.......
const buttonUp = document.querySelector('.buttonUp');

buttonUp.addEventListener('click', () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
});
window.onscroll = function() {
    scrollFunction();
};

function scrollFunction() {
    if (document.body.scrollTop > 700 || document.documentElement.scrollTop > 700){
        buttonUp.style.display = 'block';
    } else {
        buttonUp.style.display = 'none';
    }
}
//.............forms................
const forms = document.querySelectorAll('form'),
      message = {
          loading: 'Загрузка...',
          success: 'Спасибо! Скоро я с вами свяжусь',
          failure: 'Что-то пошло не так...'
      },
      submit = document.querySelector('.contact__triggers-btn');

forms.forEach(item => {
    postData(item);
});

// function postData(form){
//     form.addEventListener('submit', (e) => {
//         e.preventDefault();
//         setTimeout(() => {
//             form.reset();
//         }, 3000);

//     });
// }
// const forms = document.querySelectorAll('form'),
//       message = {
//           loading: 'Загрузка...',
//           success: 'Спасибо! Скоро я с вами свяжусь',
//           failure: 'Что-то пошло не так...'
//       },
//       submit = document.querySelector('.contact__triggers-btn');

// forms.forEach(item => {
//     postData(item);
// });

// function postData(form){
//     form.addEventListener('submit', (e) => {
//         e.preventDefault();

//         let statusMessage = document.createElement('div');
//         statusMessage.textContent = message.loading;
//         form.appendChild(statusMessage);

//         const request = new XMLHttpRequest();
//         request.open('POST', 'mail.php');
//         const formData = new FormData(form);
        
//         const object = {};
//         formData.forEach(function(value, key){
//             object[key] = value;
//         });
//         const json = JSON.stringify(object);

//         request.send(json);
//         console.log(json);

//         request.addEventListener('load', () => {
//             if(request.status === 200){
//                 console.log(request.response);
//                 statusMessage.textContent = message.success;
//                 form.reset();
//                 setTimeout(() => {
//                     statusMessage.remove();
//                 }, 3000);
//             } else{
//                 statusMessage.textContent = message.failure;
//             }
//         });
//     });
// }

// submit.addEventListener('submit', () => {
//     const form = document.querySelector('contact__form');
//     form.style.display = "none";
// });

//.................progress bar...........
let progress = document.querySelector('.progressBar');
let totalHeight = document.body.scrollHeight - window.innerHeight;

window.addEventListener('scroll', () => {
    let progressHeight = (window.pageYOffset / totalHeight) * 100;

    progress.style.height = progressHeight + "%";
});

// //............clock......
const deg = 6;
const hr = document.querySelector('#hr');
const mn = document.querySelector('#mn');
const sec = document.querySelector('#sec');



function showTime() {
    let day = new Date();
    let hh = day.getHours() * 30;
    let mm = day.getMinutes() * deg;
    let ss = day.getSeconds() * deg;

    hr.style.transform = `rotateZ(${(hh)+(mm/12)}deg)`;
    mn.style.transform = `rotateZ(${mm}deg)`;
    sec.style.transform = `rotateZ(${ss}deg)`;
}
setInterval(() => {
    showTime();
});

// //.........modal clock......

const modalClock = document.querySelector('.modal__clock'),
      closeClock = document.querySelector('.clock__close'),
      clockBtn = document.querySelector('[data-clock]');

clockBtn.addEventListener('click', (e) => {
    e.preventDefault();
    modalClock.classList.toggle('modal__clock_active');
});

closeClock.addEventListener('click', () => {
    modalClock.classList.remove('modal__clock_active');
});

//.........wow animation......
new WOW().init();